//
//  Utils.h
//  Vizor
//
//  Created by Viktor Sukochev on 07.11.15.
//  Copyright (c) 2015 Viktor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>

void showError(NSString* message, UIViewController* hostVC);

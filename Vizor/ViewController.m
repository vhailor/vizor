//
//  ViewController.m
//  Vizor
//
//  Created by Viktor Sukochev on 07.11.15.
//  Copyright (c) 2015 Viktor Sukochev. All rights reserved.
//

#import "ViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "Utils.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UIView* videoView;

@property (nonatomic, assign) BOOL              isPlaying;
@property (nonatomic, strong) CALayer*          videoLayer;
@property (nonatomic, strong) AVCaptureSession* session;

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - UI action

- (IBAction)onPlay:(UIButton*)btn
{
    if ( self.isPlaying )
    {
        [btn setTitle:@"Play" forState:UIControlStateNormal];
        [self _stop];
        return;
    }

    if ( [self _play] )
    {
        [btn setTitle:@"Stop" forState:UIControlStateNormal];
    }
}

#pragma mark - Private methods

- (BOOL) _play
{
    if ( ![self _setupInput] )
    {
        return NO;
    }
    
    if ( self.videoLayer != nil )
    {
        [self.videoLayer removeFromSuperlayer];
    }

    self.videoLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    self.videoLayer.frame = self.videoView.bounds;
    [self.videoView.layer addSublayer:self.videoLayer];
    
    [self.session startRunning];
    
    self.isPlaying = YES;
    return YES;
}

- (void) _stop
{
    [self.session stopRunning];
    
    self.session    = nil;
    
    self.isPlaying = NO;
}

- (BOOL) _setupInput
{
    self.session = [AVCaptureSession new];
    self.session.sessionPreset = AVCaptureSessionPresetMedium;
    
    NSError* error = nil;
    AVCaptureDevice*     device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if ( error != nil )
    {
        showError(error.localizedDescription, self);
        return NO;
    }
    
    if ( ![self.session canAddInput:input] )
    {
        showError(@"Camera can not be used on this device", self);
        return NO;
    }
    
    [self.session addInput:input];
    return YES;
}


@end

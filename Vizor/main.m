//
//  main.m
//  Vizor
//
//  Created by Viktor Sukochev on 07.11.15.
//  Copyright (c) 2015 Viktor Sukochev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

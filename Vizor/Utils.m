//
//  Utils.m
//  Vizor
//
//  Created by Viktor Sukochev on 07.11.15.
//  Copyright (c) 2015 Viktor Sukochev. All rights reserved.
//

#import "Utils.h"

void showError(NSString* message, UIViewController* hostVC)
{
    UIAlertController* alertVC = [UIAlertController alertControllerWithTitle:@"Error"
                                                                     message:message
                                                              preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    [hostVC presentViewController:alertVC animated:YES completion:nil];
}


